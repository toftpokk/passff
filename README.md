# PassFF
PassFF is a python script which takes passwords exported from firefox
and imports them into pass

## Usage
Edit the python script in the first section to change input file 
```python
LOGIN_CSV="logins.csv" # CSV File
PASS_PROG="pass" # Pass program
SAVE_FF_ACCOUNT=False # Save chrome://FirefoxAccounts ?
PASS_NAME_PREPEND="web/" # Prepends pass name eg. web/www.email.example
```

## Disclaimer
PassFF may overwrite old passwords or create duplicate passwords
if execution is not carefully thought through

## Configuring
To change pass name format edit the `get_pass_name` function
