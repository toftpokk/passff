#!/usr/bin/env python3

# Pass FF

# Inspired by password-exporter2pass.py

LOGIN_CSV="logins.csv" # CSV File
PASS_PROG="pass" # Pass program
SAVE_FF_ACCOUNT=False # Save chrome://FirefoxAccounts ?
PASS_NAME_PREPEND="web/" # Prepends pass name eg. web/www.email.example

import csv
import subprocess
import sys

### Helper Functions ###
def clean_url(url):
    return url.replace("/","%2f")

def get_url_name(url):
    return url.replace("https://","").replace("http://","")

def get_pass_name(password):
    return "{}{}-{}".format(PASS_NAME_PREPEND,clean_url(get_url_name(password["url"])),clean_url(password["username"]))

### Important ###
def read_csv(file_name):
    password_list = []
    with open(file_name) as f:
        raw_text = f.readlines()
        reader=csv.reader(raw_text)

    pwd_idx=0
    for i in reader:
        if pwd_idx == 0:
            # Header
            header = i
            pwd_idx+=1
            continue
        # Password
        password = {}
        for h in range(len(header)):
            # put all fields in password
            password[header[h]] = i[h]
        if not SAVE_FF_ACCOUNT:
            # Save chrome://FirefoxAccounts ?
            if password["url"] == "chrome://FirefoxAccounts": continue
        password_list.append(password)
        pwd_idx+=1
    return password_list

def text_encode(password):
    # Encode password as string of text 
    text=""
    # Url
    url = get_url_name(password["url"])

    text+="{}\n".format(password["password"])
    text+="URL: {}\n".format(url)
    text+="Username: {}".format(password["username"])
    return bytes(text,"utf-8")

def add_topass(text,pass_name):
    # Add encoded text to PASS_PROG program
    cmd = [PASS_PROG, "insert", "--multiline"]
    cmd.append(pass_name)

    proc = subprocess.Popen(
        cmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)

    stdout, stderr = proc.communicate(text)
    return proc.wait()

# ------ Main ------ #
def main():
    password_list = read_csv(LOGIN_CSV)

    for i in password_list:
        pass_text = text_encode(i)
        pass_name = get_pass_name(i)
        add_topass(pass_text,pass_name)

def test_sample():
    pass_text = \
    """test_sample_pass
URL: sample.test.com
Username: test_sample_user"""
    pass_name = get_pass_name(i)
    add_topass(pass_text,pass_name)

if __name__ == "__main__":
    main()
